package gov.mil.esp.espservicecatalog;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class EspServiceCatalogApplication {

	public static void main(String[] args) {
		SpringApplication.run(EspServiceCatalogApplication.class, args);
	}

}
